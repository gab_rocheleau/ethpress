<?xml version="1.0"?>
<ruleset name="WordPress Coding Standards based custom ruleset for your plugin">
	<description>Generally-applicable sniffs for WordPress plugins.</description>

	<!-- What to scan -->
	<file>.</file>
	<exclude-pattern>/vendor/</exclude-pattern>
	<exclude-pattern>/node_modules/</exclude-pattern>
	<exclude-pattern>/app/Dependencies/</exclude-pattern>

	<!-- How to scan -->
	<!-- Usage instructions: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Usage -->
	<!-- Annotated ruleset: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-ruleset.xml -->
	<arg value="sp"/> <!-- Show sniff and progress -->
	<arg name="basepath" value="./"/><!-- Strip the file paths down to the relevant bit -->
	<arg name="colors"/>
	<arg name="extensions" value="php"/>
	<arg name="parallel" value="8"/><!-- Enables parallel processing when available for faster results. -->

	<!-- Rules: Check PHP version compatibility -->
	<!-- https://github.com/PHPCompatibility/PHPCompatibility#sniffing-your-code-for-compatibility-with-specific-php-versions -->
	<config name="testVersion" value="5.4-"/>
	<!-- https://github.com/PHPCompatibility/PHPCompatibilityWP -->
	<rule ref="PHPCompatibilityWP"/>

	<!-- Rules: WordPress Coding Standards -->
	<!-- https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards -->
	<!-- https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/wiki/Customizable-sniff-properties -->
	<config name="minimum_supported_wp_version" value="4.6"/>
	<rule ref="WordPress">
		<!-- <exclude name="WordPress.VIP"/> -->
		<exclude name="WordPress.Files.FileName"/>
		<!-- Excluding this for now so I don't have to change em. -->
		<exclude name="Generic.Arrays.DisallowShortArraySyntax.Found"/>
	</rule>
	<rule ref="WordPress.Security.ValidatedSanitizedInput">
		<properties>
			<property name="customSanitizingFunctions" type="array">
				<!-- Address::sanitize, doesn't work with wpcs 2.0: https://github.com/WordPress/WordPress-Coding-Standards/issues/1766 -->
				<element value="sanitize"/>
				<!-- Missing from standards, I believe. -->
				<element value="wp_sanitize_redirect"/>
			</property>
		</properties>
	</rule>
	<rule ref="WordPress.NamingConventions.PrefixAllGlobals">
		<properties>
			<!-- Value: replace the function, class, and variable prefixes used. Separate multiple prefixes with a comma. -->
			<property name="prefixes" type="array" value="ethpress,losnappas\Ethpress"/>
		</properties>
	</rule>
	<rule ref="WordPress.WP.I18n">
		<properties>
			<!-- Value: replace the text domain used. -->
			<property name="text_domain" type="array" value="ethpress"/>
		</properties>
	</rule>
	<rule ref="WordPress.WhiteSpace.ControlStructureSpacing">
		<properties>
			<property name="blank_line_check" value="true"/>
		</properties>
	</rule>

</ruleset>
